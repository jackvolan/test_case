#! /usr/bin/env python
# -*- coding: utf-8 -*-
'''
Программа для получения даты, заголовка и ссылкы, на главные новости с главной странице РБК
Что бы запустить программу достаточно вызвать функцию main
Так же для работы программы необходимо иметь модуль bs4
'''

from bs4 import BeautifulSoup, Comment
import requests


def get_news_json():
    '''
    Функция возвращает json данных в котором храняться все данны о главных новостях
    '''

    req = requests.get('https://www.rbc.ru/v10/ajax/main/region/world/publicher/main_main?_=1544983378697')
    json = req.json()
    return json


def get_html_title_list():
    '''
     Функция возвращает список html кода главных новостей
    '''

    html_list = []
    json = get_news_json()
    for i in range(15):
        html_list.append(json['items'][i]['html'])
    return html_list


def del_comment(html):
    '''
    Функция принимает  html код, и удаляет в нем все комментарии
    Создана она для того что бы потом использовать метод string на объект суппа
    И получить строку с заголовком новости
    и наче же метод string вмести с коментариеями убирает и строку
    '''

    for element in html(text=lambda text: isinstance(text, Comment)):
        element.extract()


def get_url_news_list():
    '''
    Функция парсит список html кода, находит в нем все ссылки на новости и возвращает список ссылок
    '''

    html_list = get_html_title_list()
    url_list = []
    for i in range(len(html_list)):
        soup = BeautifulSoup(html_list[i], 'lxml')
        for link in soup.find_all('a'):
            url_list.append(link.get('href'))
    return url_list


def get_title_list():
    '''
    Функция парсит список html кода, находит все span с заголовками новостей, удаляет коментарии
    И оставляет только заголовки новостей, возвращает список из заголовок
    '''

    html_list = get_html_title_list()
    title_list = []
    for i in range(len(html_list)):
        soup = BeautifulSoup(html_list[i], 'lxml')
        html = soup.find('span', ['main__feed__title', 'main__big__title'])
        del_comment(html)
        title_list.append(html.string)
    return title_list


def get_id():
    '''
    Функция возвращает список id главных новостей,
    '''

    id_list = []
    json = get_news_json()
    for i in range(15):
        id_list.append(json['items'][i]['id'])
    return id_list


def get_data():
    '''
    Функция возвращает список даты публикации новостей
    Перед этим функция отправляет запрос, и находит по всем новостям нужные, с помощью id новостей
    '''

    html = requests.get('https://sportrbc.ru/v8/ajax/getnewsfeed/region/sport/?_=')
    html = html.json()
    id = get_id()
    data = []
    for i in range(15):
        for k in range(200):
            if id[i] == html['items'][k]['id']:
                data.append(html['items'][k]['time'])
                break
    return data


def output_on_display(data, title, url):
    '''
    Функция принимает список дат, заголовков, ссылок. И выводит на экран данные по строчно
    '''

    for i in range(15):
        print(data[i], '|', title[i], '|', url[i])


def output_in_file(data, title, url):
    '''
    Функция принимает список дат, заголовков, ссылок. И выводит в файл данные по строчно
    '''

    file = open('news.txt', 'w', encoding='utf8')
    for i in range(15):
        file.write(data[i] + '|' + title[i] + '|' + url[i] + '\n')
    file.close()


def main():
    print('Идет сбор данных\n')

    data = get_data()
    title = get_title_list()
    url = get_url_news_list()

    print(
        '1.Вывести данные на экран\n' + '2.Вывести данные в файл под названием "News"\n'
        + '3.Вывести на экран и в файл\n' + '4.Выйти')
    answer = input('\nВведите цифру вашего ответа: ')

    while answer != '4':
        if answer == '1':
            output_on_display(data, title, url)
        if answer == '2':
            output_in_file(data, title, url)
        if answer == '3':
            output_on_display(data, title, url)
            output_in_file(data, title, url)
        if answer != '1' or '2' or '3':
            answer = input('Введите цифру вашего ответа: ')


if __name__ == '__main__':
    main()
